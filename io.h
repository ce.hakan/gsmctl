#ifndef _IO_H
#define _IO_H

int read_INPUT(char io, unsigned char pin, unsigned char *data);
int write_OUTPUT(unsigned char pin, unsigned char data);

#endif