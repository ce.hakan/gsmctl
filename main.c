/*
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <time.h>
#include <math.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>

#include "io.h"
#include "ads1115.h"
#include "MCP9808.h"

#include "uci.h"

int fd;
char  *ret;
char *pret;
unsigned char pCnt;
unsigned char txBuffer[100];
unsigned char rxBuffer[100];
int     cloud_send_period;
char    cloud_stat[4];
char    cloud_interval[2];
char    tokenid[25];
char    property1_stat[4];
char	property1_text[25];
char	property1_inp[5];
char	property2_stat[4];
char	property2_text[25];
char	property2_inp[5];
char	alarm1_stat[4];
char	alarm1_inp[5];
char	alarm1_text[25];
char	alarm2_stat[4];
char	alarm2_inp[5];
char	alarm2_text[25];

float		MCP9808Temp;
float		channelADC[2];
unsigned char	channelGPIO[8];
unsigned char	channelGPIO_old[4];

char 	message[255];
int	indication_send_period;
char	indication_interval[2];
char hour_mod;
char hour_flag;
char send_flag;
int  destport;
float adc1min;
float adc1max;
float adc2min;
float adc2max;
float tempmin;
float tempmax;
char imei[20];
char ccid[20];
char work_mode[10];
char indication_stat[4];
//char gen_mode[4];
char id_mode[5];
char sms_ip_mode[5];
char dest_ip[20];
char dest_port[6];
char adc1_min_status[5];
char adc1_min[5];
char adc1_max_status[5];
char adc1_max[5];
char adc2_min_status[5];
char adc2_min[5];
char adc2_max_status[5];
char adc2_max[5];
char temp_min_status[5];
char temp_min[5];
char temp_max_status[5];
char temp_max[5];
char input_status[5];
char phone_number[20];

char adc1_min_trigger;
char adc1_max_trigger;
char adc2_min_trigger;
char adc2_max_trigger;
char temp_min_trigger;
char temp_max_trigger;

struct termios uart;
struct sockaddr_in their_addr;
struct  sockaddr_in si_other;

int sockfd, s;

time_t t;
struct tm tm;

int old_day;
int old_hour;
int old_cloud = 0;
int old_sec = 0;
long int time_val = 0;
int fullCont = 0;

bool i0norm = false;
bool i1norm = false;
bool i2norm = false;
bool i3norm = false;
bool initGPIO = true;
bool adc0norm = false;
bool adc1norm = false;
bool tempNorm = false;

long int cloud_time_val = 0;

static void clcBUFFER(unsigned char *pBuff, unsigned char pCnt) {
    unsigned char i;
    for(i=0; i<pCnt; i++) *pBuff++ = 0x00;
}

static void read_Peripherals(void) {
    MCP9808Temp     = read_MCP9808();

    channelADC[0]   = read_ADS1115(0);
    channelADC[0]  *= 1000;
    channelADC[1]   = read_ADS1115(1);
    channelADC[1]  *= 1000;

    read_INPUT(0,'0',&channelGPIO[0]);
    read_INPUT(0,'1',&channelGPIO[1]);
    read_INPUT(0,'2',&channelGPIO[2]);
    read_INPUT(0,'3',&channelGPIO[3]);

    read_INPUT(1,'4',&channelGPIO[4]);
    read_INPUT(1,'5',&channelGPIO[5]);
    read_INPUT(1,'6',&channelGPIO[6]);
    read_INPUT(1,'7',&channelGPIO[7]);

    if(initGPIO) {
        channelGPIO_old[0] = channelGPIO[0];
        channelGPIO_old[1] = channelGPIO[1];
        channelGPIO_old[2] = channelGPIO[2];
        channelGPIO_old[3] = channelGPIO[3];
        initGPIO = false;
    }

    if(strncmp(id_mode,"CCID",4)==0)
    {
        sprintf(message,"%s,%4.2f,%4.2f,%d%d%d%d,%d%d%d%d,%4.2f",
                ccid,
                channelADC[0],channelADC[1],
                channelGPIO[0],channelGPIO[1],channelGPIO[2],channelGPIO[3],
                channelGPIO[4],channelGPIO[5],channelGPIO[6],channelGPIO[7],
                MCP9808Temp );
    }

    if(strncmp(id_mode,"IMEI",4)==0)
    {
        sprintf(message,"%s,%4.2f,%4.2f,%d%d%d%d,%d%d%d%d,%4.2f",
                imei,
                channelADC[0],channelADC[1],
                channelGPIO[0],channelGPIO[1],channelGPIO[2],channelGPIO[3],
                channelGPIO[4],channelGPIO[5],channelGPIO[6],channelGPIO[7],
                MCP9808Temp );
    }
}

static tcp_send(void) {
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) perror("TCP socket");
    if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0) perror("setsockopt(SO_REUSEADDR) fail");

    their_addr.sin_family		= AF_INET;
    their_addr.sin_port		= htons(destport);
    their_addr.sin_addr.s_addr	= inet_addr(dest_ip);
    bzero(&(their_addr.sin_zero), 8);

    char cnt = 0;
    while(connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1) {
        perror("connect");
        usleep(300000);
        cnt++;
        if(cnt>5) goto exit;
    }

    if(send(sockfd, message, strlen(message), 0) == -1) perror("send");
    else printf("TCP Send successful.\r\n");
exit:
    close(sockfd);
}

static char udp_send(void) {
    if((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) perror("UDP socket	");

    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family	= AF_INET;
    si_other.sin_port	= htons(destport);
    if(inet_aton(dest_ip, &si_other.sin_addr) == 0) fprintf(stderr, "inet_aton() failed\n");

    if(sendto(sockfd, message, strlen(message), 0, (struct sockaddr *)&si_other, sizeof(si_other)) == -1) perror("send");
    else printf("UDP Send successful.\r\n");
}

static char sms_send(void) {
    char *ret;
    char len;
    if(fd) {
        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));
x0:
        if(pCnt>4) goto x1;
        sprintf(txBuffer, "AT+CMGF=1\r");
        write(fd, txBuffer, strlen(txBuffer));
        usleep(300000);

        if(read(fd, &rxBuffer, 50)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x0;
            }
        }
        else {
            pCnt++;
            goto x0;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));
x1:
        if(pCnt>4) goto x2;
        sprintf(txBuffer, "AT+CMGS=\"%s\"\r", phone_number);
        write(fd, &txBuffer[0], strlen(txBuffer));
        usleep(300000);

        if(read(fd, &rxBuffer, 100)) {
            ret = strstr(rxBuffer,">");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x1;
            }
        }
        else {
            pCnt++;
            goto x1;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));
x2:
        if(pCnt>4) goto exit;
        sprintf(txBuffer, "%s", message);
        len = strlen(txBuffer);
        txBuffer[len] = 0x1A;
        txBuffer[len+1] = 0x0D;
        write(fd, txBuffer, strlen(txBuffer));
        usleep(300000);

        if(read(fd, &rxBuffer, 50)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x2;
            }
        }
        else {
            pCnt++;
            goto x2;
        }
exit:
        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));
    }
    return 0;
}

char check_trigger(void) {
    read_Peripherals();
    if(strncmp(adc1_min_status, "ON", 2) == 0) {
        if(channelADC[0] < adc1min) {
            if(adc1_min_trigger) {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode,"IMEI",4)==0)
                    sprintf(message,"<%s,ADC Channel 0 Min. Alarm !,%4.2fmV>\n",imei,channelADC[0]);
                if(strncmp(id_mode,"CCID",4)==0)
                    sprintf(message,"<%s,ADC Channel 0 Min. Alarm !,%4.2fmV>\n",ccid,channelADC[0]);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                adc1_min_trigger = 0;
                adc0norm = true;
            }
        }
        else {
            if(adc1max > adc1min) {
                if(channelADC[0] < adc1max && channelADC[0] > adc1min) {
                    if(adc0norm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,ADC Channel 0 Normal State,%4.2fmV>\n",imei,channelADC[0]);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,ADC Channel 0 Normal State,%4.2fmV>\n",ccid,channelADC[0]);
                        printf("%s",message);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        adc0norm = false;
                    }
                    adc1_min_trigger = 1;
                }
            }
            else {
                if(channelADC[0] > adc1min) {
                    if(adc0norm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,ADC Channel 0 Normal State,%4.2fmV>\n",imei,channelADC[0]);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,ADC Channel 0 Normal State,%4.2fmV>\n",ccid,channelADC[0]);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        adc0norm = false;
                    }
                    adc1_min_trigger = 1;
                }
            }
        }
    }

    if(strncmp(adc1_max_status, "ON", 2) == 0) {
        if(channelADC[0] > adc1max) {
            if(adc1_max_trigger) {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode,"IMEI",4)==0)
                    sprintf(message,"<%s,ADC Channel 0 Max. Alarm !,%4.2fmV>\n",imei,channelADC[0]);
                if(strncmp(id_mode,"CCID",4)==0)
                    sprintf(message,"<%s,ADC Channel 0 Max. Alarm !,%4.2fmV>\n",ccid,channelADC[0]);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                adc1_max_trigger = 0;
                adc0norm = true;
            }
        }
        else {
            if(adc1max > adc1min) {
                if(channelADC[0] < adc1max && channelADC[0] > adc1min) {
                    if(adc0norm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,ADC Channel 0 Normal State,%4.2fmV>\n",imei,channelADC[0]);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,ADC Channel 0 Normal State,%4.2fmV>\n",ccid,channelADC[0]);
                        printf("%s",message);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        adc0norm = false;
                    }
                    adc1_max_trigger = 1;
                }
            }
            else {
                if(channelADC[0] < adc1max) {
                    if(adc0norm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,ADC Channel 0 Normal State,%4.2fmV>\n",imei,channelADC[0]);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,ADC Channel 0 Normal State,%4.2fmV>\n",ccid,channelADC[0]);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        adc0norm = false;
                    }
                    adc1_max_trigger = 1;
                }
            }
        }
    }

    if(strncmp(adc2_min_status, "ON", 2) == 0) {
        if(channelADC[1] < adc2min) {
            if(adc2_min_trigger) {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode,"IMEI",4)==0)
                    sprintf(message,"<%s,ADC Channel 1 Min. Alarm !,%4.2fmV>\n",imei,channelADC[1]);
                if(strncmp(id_mode,"CCID",4)==0)
                    sprintf(message,"<%s,ADC Channel 1 Min. Alarm !,%4.2fmV>\n",ccid,channelADC[1]);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                adc2_min_trigger = 0;
                adc1norm = true;
            }
        }
        else {
            if(adc2max > adc2min) {
                if(channelADC[1] < adc2max && channelADC[1] > adc2min) {
                    if(adc1norm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,ADC Channel 1 Normal State,%4.2fmV>\n",imei,channelADC[1]);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,ADC Channel 1 Normal State,%4.2fmV>\n",ccid,channelADC[1]);
                        printf("%s",message);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        adc1norm = false;
                    }
                    adc2_min_trigger = 1;
                }
            }
            else {
                if(channelADC[1] > adc2min) {
                    if(adc1norm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,ADC Channel 1 Normal State,%4.2fmV>\n",imei,channelADC[1]);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,ADC Channel 1 Normal State,%4.2fmV>\n",ccid,channelADC[1]);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        adc1norm = false;
                    }
                    adc2_min_trigger = 1;
                }
            }
        }
    }

    if(strncmp(adc2_max_status, "ON", 2) == 0) {
        if(channelADC[1] > adc1max) {
            if(adc2_max_trigger) {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode,"IMEI",4)==0)
                    sprintf(message,"<%s,ADC Channel 1 Max. Alarm !,%4.2fmV>\n",imei,channelADC[1]);
                if(strncmp(id_mode,"CCID",4)==0)
                    sprintf(message,"<%s,ADC Channel 1 Max. Alarm !,%4.2fmV>\n",ccid,channelADC[1]);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                adc2_max_trigger = 0;
                adc1norm = true;
            }
        }
        else {
            if(adc2max > adc2min) {
                if(channelADC[1] < adc2max && channelADC[1] > adc2min) {
                    if(adc1norm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,ADC Channel 1 Normal State,%4.2fmV>\n",imei,channelADC[1]);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,ADC Channel 1 Normal State,%4.2fmV>\n",ccid,channelADC[1]);
                        printf("%s",message);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        adc1norm = false;
                    }
                    adc2_max_trigger = 1;
                }
            }
            else {
                if(channelADC[1] < adc2max) {
                    if(adc1norm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,ADC Channel 1 Normal State,%4.2fmV>\n",imei,channelADC[1]);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,ADC Channel 1 Normal State,%4.2fmV>\n",ccid,channelADC[1]);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        adc1norm = false;
                    }
                    adc2_max_trigger = 1;
                }
            }
        }
    }

    if(strncmp(temp_min_status, "ON", 2) == 0) {
        if(MCP9808Temp < tempmin) {
            if(temp_min_trigger) {
                clcBUFFER(message,sizeof(message));
                if(strncmp(id_mode,"IMEI",4)==0)
                    sprintf(message,"<%s,Temperature Min. Alarm !,%4.2fC>\n",imei,MCP9808Temp);
                if(strncmp(id_mode,"CCID",4)==0)
                    sprintf(message,"<%s,Temperature Min. Alarm !,%4.2fC>\n",ccid,MCP9808Temp);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                temp_min_trigger = 0;
                tempNorm = true;
            }
        }
        else {
            if(tempmax > tempmin) {
                if(MCP9808Temp > tempmin && MCP9808Temp < tempmax) {
                    if(tempNorm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,Temperature Normal State,%4.2fC>\n",imei,MCP9808Temp);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,Temperature Normal State,%4.2fC>\n",ccid,MCP9808Temp);
                        printf("%s",message);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        tempNorm = false;
                    }
                    temp_min_trigger = 1;
                }
            }
            else {
                if(MCP9808Temp > tempmin) {
                    if(tempNorm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,Temperature Normal State,%4.2fC>\n",imei,MCP9808Temp);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,Temperature Normal State,%4.2fC>\n",ccid,MCP9808Temp);
                        printf("%s",message);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        tempNorm = false;
                    }
                    temp_min_trigger = 1;
                }
            }
        }
    }

    if(strncmp(temp_max_status, "ON", 2) == 0) {
        if(MCP9808Temp > tempmax) {
            if(temp_max_trigger) {
                clcBUFFER(message,sizeof(message));
                if(strncmp(id_mode,"IMEI",4)==0)
                    sprintf(message,"<%s,Temperature Max. Alarm !,%4.2fC>\n",imei,MCP9808Temp);
                if(strncmp(id_mode,"CCID",4)==0)
                    sprintf(message,"<%s,Temperature Max. Alarm !,%4.2fC>\n",ccid,MCP9808Temp);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                temp_max_trigger = 0;
                tempNorm = true;
            }
        }
        else {
            if(tempmax > tempmin) {
                if(MCP9808Temp > tempmin && MCP9808Temp < tempmax) {
                    if(tempNorm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,Temperature Normal State,%4.2fC>\n",imei,MCP9808Temp);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,Temperature Normal State,%4.2fC>\n",ccid,MCP9808Temp);
                        printf("%s",message);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        tempNorm = false;
                    }
                    temp_max_trigger = 1;
                }
            }
            else {
                if(MCP9808Temp < tempmax) {
                    if(tempNorm) {
                        clcBUFFER(message,sizeof(message));
                        if(strncmp(id_mode,"IMEI",4)==0)
                            sprintf(message,"<%s,Temperature Normal State,%4.2fC>\n",imei,MCP9808Temp);
                        if(strncmp(id_mode,"CCID",4)==0)
                            sprintf(message,"<%s,Temperature Normal State,%4.2fC>\n",ccid,MCP9808Temp);
                        printf("%s",message);

                        if(strncmp(sms_ip_mode,"TCP",3)==0) tcp_send();
                        if(strncmp(sms_ip_mode,"UDP",3)==0) udp_send();
                        if(strncmp(sms_ip_mode,"SMS",3)==0) sms_send();
                        tempNorm = false;
                    }
                    temp_max_trigger = 1;
                }
            }
        }
    }

    if(strncmp(input_status, "ON", 2) == 0) {
        if(channelGPIO[0] != channelGPIO_old[0]) {
            if(channelGPIO[0] == '1') {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode, "IMEI", 4) == 0) sprintf(message,"<%s,Input0 Alarm !>\n",imei);
                if(strncmp(id_mode, "CCIF", 4) == 0) sprintf(message,"<%s,Input0 Alarm !>\n",ccid);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                i0norm = true;
            }
            else if(i0norm) {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode, "IMEI", 4) == 0) sprintf(message,"<%s,Input0 Normal>\n",imei);
                if(strncmp(id_mode, "CCIF", 4) == 0) sprintf(message,"<%s,Input0 Normal>\n",ccid);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                i0norm = false;
            }
        }

        if(channelGPIO[1] != channelGPIO_old[1]) {
            if(channelGPIO[1] == '1') {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode, "IMEI", 4) == 0) sprintf(message,"<%s,Input1 Alarm !>\n",imei);
                if(strncmp(id_mode, "CCIF", 4) == 0) sprintf(message,"<%s,Input1 Alarm !>\n",ccid);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                i1norm = true;
            }
            else if(i1norm) {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode, "IMEI", 4) == 0) sprintf(message,"<%s,Input1 Normal>\n",imei);
                if(strncmp(id_mode, "CCIF", 4) == 0) sprintf(message,"<%s,Input1 Normal>\n",ccid);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                i1norm = false;
            }
        }

        if(channelGPIO[2] != channelGPIO_old[2]) {
            if(channelGPIO[2] == '1') {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode, "IMEI", 4) == 0) sprintf(message,"<%s,Input2 Alarm !>\n",imei);
                if(strncmp(id_mode, "CCIF", 4) == 0) sprintf(message,"<%s,Input2 Alarm !>\n",ccid);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                i2norm = true;
            }
            else if(i2norm) {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode, "IMEI", 4) == 0) sprintf(message,"<%s,Input2 Normal>\n",imei);
                if(strncmp(id_mode, "CCIF", 4) == 0) sprintf(message,"<%s,Input2 Normal>\n",ccid);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                i2norm = false;
            }
        }

        if(channelGPIO[3] != channelGPIO_old[3]) {
            if(channelGPIO[3] == '1') {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode, "IMEI", 4) == 0) sprintf(message,"<%s,Input3 Alarm !>\n",imei);
                if(strncmp(id_mode, "CCIF", 4) == 0) sprintf(message,"<%s,Input3 Alarm !>\n",ccid);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                i3norm = true;
            }
            else if(i3norm) {
                clcBUFFER(message, sizeof(message));
                if(strncmp(id_mode, "IMEI", 4) == 0) sprintf(message,"<%s,Input3 Normal>\n",imei);
                if(strncmp(id_mode, "CCIF", 4) == 0) sprintf(message,"<%s,Input3 Normal>\n",ccid);

                if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
                if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
                if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
                i3norm = false;
            }
        }
    }

    channelGPIO_old[0] = channelGPIO[0];
    channelGPIO_old[1] = channelGPIO[1];
    channelGPIO_old[2] = channelGPIO[2];
    channelGPIO_old[3] = channelGPIO[3];
}

char check_time(void) {
    t = time(NULL);
    tm = *localtime(&t);

    send_flag = 0;

    time_val += (tm.tm_sec);
    time_val += (tm.tm_min * 60);
    time_val += (tm.tm_hour * 3600);
    time_val += (tm.tm_yday * 216000);

    if((old_sec + ((indication_send_period-1)*60) - 5) <= time_val%(indication_send_period*60) || time_val%(indication_send_period*60) <= (old_sec + ((indication_send_period-1)*60) + 5)) {
        old_sec = tm.tm_sec;
        send_flag = 1;
    }

    if(send_flag) {
        read_Peripherals();

        if(strncmp(sms_ip_mode, "UDP", 3) == 0) udp_send();
        if(strncmp(sms_ip_mode, "TCP", 3) == 0) tcp_send();
        if(strncmp(sms_ip_mode, "SMS", 3) == 0) sms_send();
        send_flag = 0;
    }
}

static char init_CLOUD(void) {
    if(fd>0) {
        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT+CPIN?\r");
x0:
        if(pCnt>4) goto x1;
        write(fd, &txBuffer[0], 9);
        usleep(300000);

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "READY");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x0;
            }
        }
        else {
            pCnt++;
            goto x0;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT+CEREG?\r");
x1:
        if(pCnt>4) goto x2;
        write(fd, &txBuffer[0], 10);
        usleep(300000);

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "+CERED: 0,1");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x1;
            }
        }
        else {
            pCnt++;
            goto x1;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT#SGACT=2,0\r");
x2:
        if(pCnt>4) goto x3;
        write(fd, &rxBuffer[0], 13);
        usleep(300000);

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x2;
            }
        }
        else {
            pCnt++;
            goto x2;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT+CGDCONT=2,\"IP\",\"internet\"\r");
x3:
        if(pCnt>4) goto x4;
        write(fd, &txBuffer[0], 29);
        usleep(300000);

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x3;
            }
        }
        else {
            pCnt++;
            goto x3;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT#SGACT=2,1\r");
x4:
        if(pCnt>4) goto x5;
        write(fd, &txBuffer[0], 13);
        usleep(2000000); // Change 3 or 4 seconds

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x4;
            }
        }
        else {
            pCnt++;
            goto x4;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT#DWCFG=\"api-dev.devicewise.com\",0,\"%s\",0,60,2,0,4,5,2\r", tokenid);
x5:
        if(pCnt>4) goto exit;
        write(fd, &txBuffer[0], 71);
        usleep(500000);

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x5;
            }
        }
        else {
            pCnt++;
            goto x5;
        }
exit:
        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

    }
    else {
        printf("ttyACM could not open\n");
        return -1;
    }
    return 0;
}

static char send_value(char *text, unsigned char channelGPIO, float channelADC, bool type) {
    if(fd>0) {
        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT#DWEN=0,0\r");
x0:
        if(pCnt>4) goto x1;
        write(fd, txBuffer, 12);
        usleep(300000);

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x0;
            }
        }
        else {
            pCnt++;
            goto x0;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT#DWEN=0,1\r");
x1:
        if(pCnt>4) goto x2;
        write(fd, &txBuffer[0], 12);
        usleep(300000);

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x1;
            }
        }
        else {
            pCnt++;
            goto x1;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT#DWCONN=0\r");
x2:
        if(pCnt>4) goto x3;
        write(fd, &txBuffer[0], 12);
        usleep(1000000);

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x2;
            }
        }
        else {
            pCnt++;
            goto x2;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT#DWCONN=1\r");
x3:
        if(pCnt>4) goto x4;
        write(fd, &txBuffer[0], 12);
        usleep(1000000);

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x3;
            }
        }
        else {
            pCnt++;
            goto x3;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        if(type) sprintf(txBuffer, "AT#DWSEND=0,property.publish,key,%s,value,%4.2f\r", text, channelADC);
        else sprintf(txBuffer, "AT#DWSEND=0,alarm.publish,key,%s,state,%d\r", text, channelGPIO);
x4:
        if(pCnt>4) goto exit;
        write(fd, txBuffer, strlen(txBuffer));
        usleep(1000000);

        pret = read(fd, &rxBuffer[0], 100);
        if(pret>0) {
            ret = strstr(rxBuffer, "");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto x4;
            }
        }
        else {
            pCnt++;
            goto x4;
        }
exit:
        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));
    }
    else {
        printf("ttyACM could not open\n");
        return -1;
    }
    return 0;
}

static char read_initial_cellular_data() {
    char IMEI[15] = "";
    char manf[5] = "";
    char modemModel[15] = "";
    char modemFW[10] = "";
    char command[200] = "";
    if(fd>0) {
        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT\r");
xz:
        if(pCnt>4) goto xy;
        write(fd, txBuffer, 3);
        usleep(300000);

        if(read(fd, &rxBuffer, 50)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto xz;
            }
        }
        else {
            pCnt++;
            goto xz;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "ATE0\r");
xy:
        if(pCnt>4) goto xa;
        write(fd, txBuffer, 5);
        usleep(300000);

        if(read(fd, &rxBuffer, 50)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) printf("OK\r");
            else {
                pCnt++;
                goto xy;
            }
        }
        else {
            pCnt++;
            goto xy;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT+CGSN\r");
xa:
        if(pCnt>4) goto xb;
        write(fd, txBuffer, 8);
        usleep(300000);

        if(read(fd, &rxBuffer, 200)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) {
                int i;
                clcBUFFER(IMEI, sizeof(IMEI));
                for(i=2; i<17; i++) IMEI[i-2] = rxBuffer[i];
                sprintf(command, "uci set system.module.imei=%s", IMEI);
                system(command);
                clcBUFFER(command, sizeof(command));
            }
            else {
                pCnt++;
                goto xa;
            }
        }
        else {
            pCnt++;
            goto xa;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT+GMI\r");
xb:
        if(pCnt>4) goto xc;
        write(fd, txBuffer, 7);
        usleep(300000);

        if(read(fd, &rxBuffer, 200)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) {
                int i;
                clcBUFFER(manf, sizeof(manf));
                for(i=2; i<7; i++) manf[i-2] = rxBuffer[i];
                sprintf(command, "uci set system.module.modemmnf=%s", manf);
                system(command);
                clcBUFFER(command, sizeof(command));
            }
            else {
                pCnt++;
                goto xb;
            }
        }
        else {
            pCnt++;
            goto xb;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT+GMM\r");
xc:
        if(pCnt>4) goto xd;
        write(fd, txBuffer, 7);
        usleep(300000);

        if(read(fd, &rxBuffer, 200)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) {
                int i;
                clcBUFFER(modemModel, sizeof(modemModel));
                for(i=2; i<13; i++) modemModel[i-2] = rxBuffer[i];
                sprintf(command,"uci set system.module.modemmodel=\'%s\'",modemModel);
                system(command);
                clcBUFFER(command, sizeof(command));
            }
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT+GMR\r");
xd:
        if(pCnt>4) goto exit;
        write(fd, txBuffer, 7);
        usleep(300000);

        if(read(fd, &rxBuffer, 200)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) {
                int i;
                clcBUFFER(modemFW, sizeof(modemFW));
                for(i=2; i<11; i++) modemFW[i-2] = rxBuffer[i];
                sprintf(command, "uci set system.module.modemFW=%s", modemFW);
                system(command);
                clcBUFFER(command, sizeof(command));
            }
            else {
                pCnt++;
                goto xd;
            }
        }
        else {
            pCnt++;
            goto xd;
        }
exit:
        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));
        system("uci commit system");
    }
    else {
        return -1;
    }
    return 0;
}

static char read_instant_cellular_data() {
    char *sim_stat;
    char *conn_type;
    char *signal;
    char *operator;
            char ccid[20] = "";
    char command[200] = "";
    if(fd>0) {
        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT#QSS?\r");
x0:
        if(pCnt>4) goto x2;
        write(fd, txBuffer, 8);
        usleep(300000);

        if(read(fd, &rxBuffer, 50)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) {
                char mode = rxBuffer[8];
                char stat = rxBuffer[10];
                if(mode == '0') {
                    if(stat == '0') sim_stat = "not_inserted";
                    else if(stat == '1') {
                        pCnt = 0;
                        clcBUFFER(txBuffer, sizeof(txBuffer));
                        clcBUFFER(rxBuffer, sizeof(rxBuffer));

                        sprintf(txBuffer, "AT+CPIN?\r");
x1:
                        if(pCnt>4) goto x2;
                        write(fd, txBuffer, 9);
                        usleep(300000);

                        if(read(fd, &rxBuffer, 50)) {
                            ret = strstr(rxBuffer, "OK");
                            if(ret != NULL) {
                                ret = strstr(rxBuffer, "READY");
                                if(ret != NULL) sim_stat = "READY";
                                else sim_stat = "inserted (NOT_READY)";
                            }
                            else {
                                pCnt++;
                                goto x1;
                            }
                        }
                        else {
                            pCnt++;
                            goto x1;
                        }
                    }
                }
                sprintf(command, "uci set realdata.realdata.sim_stat=%s", sim_stat);
                system(command);
                clcBUFFER(command, sizeof(command));
            }
            else {
                pCnt++;
                goto x0;
            }
        }
        else {
            pCnt++;
            goto x0;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT+CCID\r");
x2:
        if(pCnt>4) goto x3;
        write(fd, txBuffer, 8);
        usleep(300000);

        if(read(fd, &rxBuffer, 200)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) {
                int i;
                for(i=9; i<28; i++) ccid[i-9] = rxBuffer[i];
                sprintf(command, "uci set realdata.realdata.ccid=%s", ccid);
                system(command);
                clcBUFFER(command, sizeof(command));
            }
            else {
                pCnt++;
                goto x2;
            }
        }
        else {
            pCnt++;
            goto x2;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT#PSNT?\r");
x3:
        if(pCnt>4) goto x4;
        write(fd, txBuffer, 9);
        usleep(300000);

        if(read(fd, &rxBuffer, 50)) {
            ret = strstr(rxBuffer, "OK");
            if(ret != NULL) {
                char mode = rxBuffer[9];
                char stat = rxBuffer[11];
                if(mode == '0') {
                    if(stat == '0') conn_type = "GPRS";
                    else if(stat == '1') conn_type = "EGPRS";
                    else if(stat == '2') conn_type = "WCDMA";
                    else if(stat == '3') conn_type = "HSDPA";
                    else if(stat == '4') conn_type = "LTE";
                    else if(stat == '5') conn_type = "UNKNOWN";
                }
                sprintf(command, "uci set realdata.realdata.conn_type=%s", conn_type);
                system(command);
                clcBUFFER(command, sizeof(command));
            }
            else {
                pCnt++;
                goto x3;
            }
        }
        else {
            pCnt++;
            goto x3;
        }

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));

        sprintf(txBuffer, "AT#SERVINFO\r");
x4:
        if(pCnt>4) goto exit;
        write(fd, txBuffer, 12);
        usleep(300000);

        if(read(fd, &rxBuffer, 100)) {
            ret = strstr(rxBuffer, "#SERVINFO:");
            if(ret != NULL) {
                char *p, *p2, *saver, *saver2, *par, *par2;
                int j = 0;
                for(par = ret; ; par = NULL) {
                    p = strtok_r(par, ",", &saver);
                    if(p == NULL) break;
                    if(j == 1) {
                        signal = p;
                        j++;
                        sprintf(command, "uci set realdata.realdata.signal=%s", signal);
                        system(command);
                        clcBUFFER(command, sizeof(command));
                        continue;
                    }
                    if(j == 2) {
                        for(par2 = p; ; par2 = NULL) {
                            p2 = strtok_r(par2, "\"", &saver2);
                            if(p2 == NULL) break;
                            operator = p2;
                            sprintf(command, "uci set realdata.realdata.operator=%s", operator);
                            system(command);
                            clcBUFFER(command, sizeof(command));
                        }
                        break;
                    }
                    j++;
                }
            }
            else {
                pCnt++;
                goto x4;
            }
        }
        else {
            pCnt++;
            goto x4;
        }
exit:
        system("uci commit realdata");

        pCnt = 0;
        clcBUFFER(txBuffer, sizeof(txBuffer));
        clcBUFFER(rxBuffer, sizeof(rxBuffer));
    }
    else {
        return -1;
    }
    return 0;
}

static char init_UART(void) {
    if(fd>0) {
        bzero(&uart, sizeof(uart));

        tcgetattr(fd, &uart);
        uart.c_lflag &= ~ICANON;
        uart.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
        uart.c_iflag = IGNPAR | ICRNL;
        uart.c_oflag = 0;
        uart.c_lflag = 0;
        uart.c_cc[CTIME] = 20;
        uart.c_cc[VMIN] = 0;

        tcflush(fd, TCIFLUSH);
        tcsetattr(fd, TCSANOW, &uart);
    }
    else {
        printf("Port initialize ERROR\r");
        return -1;
    }
    return 0;
}

void stun_uci_read(const char *path, char *returnBuffer) {
    struct uci_ptr ptr;
    struct uci_context *c = uci_alloc_context();
    int UCI_LOOKUP_COMPLETE = (1<<1);

    if(!c) return;

    if((uci_lookup_ptr(c, &ptr, path, true) != UCI_OK) || (ptr.o==NULL || ptr.o->v.string==NULL)) {
        uci_free_context(c);
        return;
    }

    if(ptr.flags & UCI_LOOKUP_COMPLETE) strcpy(returnBuffer, ptr.o->v.string);

    uci_free_context(c);
}

int main(int argc, char *argv[]) {
    fd = open("/dev/ttyACM3", O_RDWR);
    init_UART();
    read_initial_cellular_data();
    while(1) {
        read_instant_cellular_data();
        stun_uci_read("cloud.cloud.enabled", cloud_stat);
        stun_uci_read("persend.persend.enabled", indication_stat);
        if(cloud_stat == '1') {
            if(fullCont == 0 || fullCont == 2) {
                init_CLOUD();
                fullCont = 1;
            }
            t = time(NULL);
            tm = *localtime(&t);

            cloud_time_val += (tm.tm_sec);
            cloud_time_val += (tm.tm_min * 60);
            cloud_time_val += (tm.tm_hour * 3600);
            cloud_time_val += (tm.tm_yday * 216000);

            stun_uci_read("cloud.cloud.period", cloud_interval);
            cloud_send_period = atoi(cloud_interval);

            if((old_cloud + ((cloud_send_period-1)*60) - 5) <= cloud_time_val%(cloud_send_period*60) || cloud_time_val%(cloud_send_period*60) <= (old_cloud + ((cloud_send_period-1)*60) + 5)) {
                read_Peripherals();
                old_cloud = tm.tm_sec;
                stun_uci_read("cloud.cloud.tokenid", tokenid);
                stun_uci_read("cloud.cloud.p1en", property1_stat);
                if(property1_stat == '1') {
                    stun_uci_read("cloud.cloud.p1pin", property1_inp);
                    stun_uci_read("cloud.cloud.p1text", property1_text);
                    if(strncmp(property1_inp, "ADC0", 4)==0) send_value(property1_text, 0, channelADC[0], true);
                    else if(strncmp(property1_inp, "ADC1", 4)==0) send_value(property1_text, 0, channelADC[1], true);
                    else if(strncmp(property1_inp, "TEMP", 4)==0) send_value(property1_text, 0, MCP9808Temp, true);
                }
                stun_uci_read("cloud.cloud.p2en", property2_stat);
                if(property2_stat == '1') {
                    stun_uci_read("cloud.cloud.p2pin", property2_inp);
                    stun_uci_read("cloud.cloud.p2text", property2_text);
                    if(strncmp(property2_inp, "ADC0", 4)==0) send_value(property2_text, 0, channelADC[0], true);
                    else if(strncmp(property2_inp, "ADC1", 4)==0) send_value(property2_text, 0, channelADC[1], true);
                    else if(strncmp(property2_inp, "TEMP", 4)==0) send_value(property2_text, 0, MCP9808Temp, true);
                }
                stun_uci_read("cloud.cloud.a1en", alarm1_stat);
                if(alarm1_stat == '1') {
                    stun_uci_read("cloud.cloud.a1pin", alarm1_inp);
                    stun_uci_read("cloud.cloud.a1text", alarm1_text);
                    if(strncmp(alarm1_inp, "INP0", 4)==0) send_value(alarm1_text, channelGPIO[0], 0, false);
                    else if(strncmp(alarm1_inp, "INP1", 4)==0) send_value(alarm1_text, channelGPIO[1], 0, false);
                    else if(strncmp(alarm1_inp, "INP2", 4)==0) send_value(alarm1_text, channelGPIO[2], 0, false);
                    else if(strncmp(alarm1_inp, "INP3", 4)==0) send_value(alarm1_text, channelGPIO[3], 0, false);
                }
                stun_uci_read("cloud.cloud.a2en", alarm2_stat);
                if(alarm2_stat == '1') {
                    stun_uci_read("cloud.cloud.a2pin", alarm2_inp);
                    stun_uci_read("cloud.cloud.a2text", alarm2_text);
                    if(strncmp(alarm1_inp, "INP0", 4)==0) send_value(alarm1_text, channelGPIO[0], 0, false);
                    else if(strncmp(alarm1_inp, "INP1", 4)==0) send_value(alarm1_text, channelGPIO[1], 0, false);
                    else if(strncmp(alarm1_inp, "INP2", 4)==0) send_value(alarm1_text, channelGPIO[2], 0, false);
                    else if(strncmp(alarm1_inp, "INP3", 4)==0) send_value(alarm1_text, channelGPIO[3], 0, false);
                }

            }


        }
        else if(indication_stat == '1') {
            if(fullCont == 0 || fullCont == 1) fullCont = 2;
            stun_uci_read("persend.persend.indtype", work_mode);
            stun_uci_read("persend.persend.idmode", id_mode);
            stun_uci_read("persend.persend.indmode", sms_ip_mode);
            if(strncmp(sms_ip_mode, "SMS", 3)==0) stun_uci_read("persend.persend.phone", phone_number);
            else if(strncmp(sms_ip_mode, "TCP", 3)==0 || strncmp(sms_ip_mode, "UDP", 3)==0) {
                stun_uci_read("persend.persend.destip", dest_ip);
                stun_uci_read("persend.persend.destport", dest_port);
                destport = atoi(dest_port);
            }
            stun_uci_read("persend.persend.a0minen", adc1_min_status);
            if(adc1_min_status == '1') {
                stun_uci_read("persend.persend.a0minval", adc1_min);
                adc1min = atoi(adc1_min);
            }
            stun_uci_read("persend.persend.a0maxen", adc1_max_status);
            if(adc1_max_status == '1') {
                stun_uci_read("persend.persend.a0maxval", adc1_max);
                adc1max = atoi(adc1_max);
            }
            stun_uci_read("persend.persend.a1minen", adc2_min_status);
            if(adc2_min_status == '1') {
                stun_uci_read("persend.persend.a1minval", adc2_min);
                adc2min = atoi(adc2_min);
            }
            stun_uci_read("persend.persend.a1maxen", adc2_max_status);
            if(adc2_max_status == '1') {
                stun_uci_read("persend.persend.a1maxval", adc2_max);
                adc2max = atoi(adc2_max);
            }
            stun_uci_read("persend.persend.tempminen", temp_min_status);
            if(temp_min_status == '1') {
                stun_uci_read("persend.persend.tempminval", temp_min);
                tempmin = strtof(temp_min, NULL);
            }
            stun_uci_read("persend.persend.tempmaxen", temp_max_status);
            if(temp_max_status == '1') {
                stun_uci_read("persend.persend.tempmaxval", temp_max);
                tempmax = strtof(temp_max, NULL);
            }
            stun_uci_read("persend.persend.inpen", input_status);
            if(strncmp(work_mode, "periodic", 8)==0) {
                stun_uci_read("persend.persend.period", indication_interval);
                indication_send_period = atoi(indication_interval);
                check_time();
            }
            else if(strncmp(work_mode, "trigger", 7)==0) check_trigger();
        }
        else fullCont = 0;
    }
}

